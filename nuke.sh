#!/bin/bash
echo " "
echo "**********************************************"
echo "                    DANGER                    "
echo "**********************************************"
echo " "
echo "You are about to destroy and remove PERMANENTLY all data for this service"
echo "If this is a mistake, please hit CTRL + C or say \"n\" to the question below."
echo " "
echo " "

read -p "Do you want to permanently delete this service (y/n)? " -n 1 -r
echo    # (optional) move to a new line

if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "Deleting..."
    export $(cat .env | xargs)
    ./down.sh
    docker volume rm $RAINLOOP_CONTAINER_VOLUME_DATA
    docker volume rm $DB_CONTAINER_VOLUME_DATA
    echo "Done !"
else
    echo "Cancelled."
fi
